FROM node:14.16.0-alpine as build

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY app/package.json ./
COPY app/package-lock.json ./

RUN npm ci --silent
RUN npm install react-scripts@4.0.3 -g --silent
COPY app ./

RUN npm run build

# Application container
FROM node:14.16.0-alpine

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
ENV NODE_ENV=production

RUN npm install -g serve
COPY --from=build /app/build /app/build

EXPOSE 5000
CMD ["serve", "-s", "build"]
